/**
 * @file eval.cc
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contain implementation of the eval function
 */

#include <iostream>
#include "object.hh"
#include "read.hh"
#include "environment.hh"
#include "defs.hh"
#include "apply.hh"

using namespace std;

Object eval(Object l, Environment *env)
{
    //cout << "Executing eval..." << l <<  endl;
    if (null(l))
    {
        // cout << "null " << l << endl;
        return l;
    }
    else if (eq(l, t()))
    {
        // cout << "true " << l << endl;
        return l;
    }
    else if (stringp(l))
    {
        // cout << "string " << l << endl;
        return l;
    }
    else if (numberp(l))
    {
        // cout << "number " << l << endl;
        return l;
    }
    else if (symbolp(l))
    {
        // cout << "symbol " << l << endl;
        try
        {
            return env->value(l);
        }
        catch (runtime_error e)
        {
            // cout << "Erreur, valeur hors mémoire" << endl;
            throw string("Undefined symbol!");
        }
    }
    // We assume the object is a list
    else
    {
        // cout << "debug : " << car(l) << endl;
        if ((symbolp(car(l))) && Object_to_string(car(l)) == "lambda") //PAS SUPER PROPRE
        {
            return cons(lisp_lambda(env), cdr(l));
        }
        /*else if (car(l) == lisp_let())
        {
            ///TODO let in eval
            return eval(car(l), env);
        }*/
        else if (eq(car(l), lisp_printenv()))
        {
            printenv(env);
            return (string_to_Object(""));
        }
        else if (eq(car(l), lisp_print()))
        {
            cout << do_print(cdr(l), env) << endl;
            return do_print(cdr(l), env);
        }
        else if (eq(car(l), lisp_read()))
        {
            return do_read();
        }
        else if (eq(car(l), lisp_quote()))
        {
            //cout << "DEBUG : quote " << cadr(l) << endl;
            return cadr(l);
        }
        else if (eq(car(l), lisp_if()))
        {
            Object o = cdr(l);
            return do_if(o, env);
        }
        else if (eq(car(l), lisp_cond()))
        {
            Object o = cdr(l);
            return do_cond(o, env);
        }
        else if (eq(car(l), lisp_andthen()))
        {
            Object o = cdr(l);
            return do_andthen(o, env);
        }
        else if (eq(car(l), lisp_progn()))
        {
            Object o = cdr(l);
            return do_progn(o, env);
        }

        else if (eq(car(l), lisp_eval()))
        {
            Object o = cadr(l);
            return do_eval(o, env);
        }
        
        else if (eq(car(l), lisp_apply()))
        {
            Object f = cadr(l);
            Object arg = caddr(l);
            return do_apply(f,arg,env);
        }

        else if (eq(car(l),lisp_defun()))
        {
            Object f = cadr(l);
            Object args = caddr(l);
            Object op = cadddr(l);
            return do_defun(f,args,op,env);
        }
        // test de macro
        else if (eq(car(l), lisp_let()))
        {
            cout << 1111 << endl;
            return (macro_let_to_lambda(l));
        }

        else if (eq(car(l), lisp_define()))
        {
            // cout << "DEBUG : setq " << l << endl;
            env->extend(cadr(l), eval(caddr(l), env)); //Modifier pour ajouter un eval
            cout << "DEFINE: " << Object_to_string(cadr(l)) << " = " << eval(cadr(l), env) << endl;       //string_to_Object(Object_to_string(cadr(l)) + " := " + Object_to_string(eval(cadr(l), env)));
            return string_to_Object("");
        }
        else if (eq(car(l), lisp_setq()))
        {
            // cout << "DEBUG : setq " << l << endl;
            env->modify(cadr(l), eval(caddr(l), env)); //Modifier pour ajouter un eval
            cout << "SET: " << Object_to_string(cadr(l)) << " = " << eval(cadr(l), env) << endl;       //string_to_Object(Object_to_string(cadr(l)) + " := " + Object_to_string(eval(cadr(l), env)));
            return string_to_Object("");
        }
        else if (eq(car(l),lisp_end()))
        {
            return(do_end());
        }

        else
        {
            // cout << "apply " << car(l) << " to " << cdr(l) << endl;
            return apply(car(l), eval_list(cdr(l), env), env); //APPLY CAR L
        }
    }
}