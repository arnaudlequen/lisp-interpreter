/**
 * @file cell.hh
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contains definitions of Cell classes
 **/
#pragma once

#include <string>

class Environment;

/*****************************/

/**
 * @class Cell
 * @brief A Cell class contain a word from the program given
 **/
class Cell
{
  public:
	/**
	 * @enum sort
	 * @brief List the different types of objects in the system
	 **/
	enum class sort
	{
		  NUMBER 	/*! A Number item */
		, STRING 	/*! A String item */
		, SYMBOL 	/*! A Symbol item */
		, PAIR  	/*! A Pair item */
		, SUBR		/*! A Subroutine item */
	};

	/**
	 * @brief Give the type of cell of the object
	 * @return The type of object
	 */
	virtual sort get_sort() const = 0;


	/**
	 * @brief Convert the cell's content to string
	 * @return A string representation of the content of the cell
	 */
	virtual std::string to_string() const = 0;
	
};

/*****************************/
/**
 * @class Cell_Number
 * @brief A Cell_Number class contain a Number item
 * @inherit Cell
 * 
 * @see Cell
 **/
class Cell_Number : public Cell
{
  private:
	int contents;

  public:
	/**
	 * @brief Give the type of cell of the object
	 * @return The type of object
	 * 
	 * @see Cell::get_sort
	 */
	sort get_sort() const override;

		/**
	 * @brief Convert the cell's content to string
	 * @return A string representation of the content of the cell
	 * 
	 * @see Cell:to_string
	 */
	std::string to_string() const override;

	/**
	 * @brief A constructor for a Cell who contain a number
	 * @param _contents The content to add in the cell
	 * @return A Cell containing the number given
	 **/
	explicit Cell_Number(int _contents);
	/**
	 * @brief Give the content of the Cell
	 * @return the value of the number in the Cell
	 **/
	int get_contents() const;
};

/*****************************/
/**
 * @class Cell_String
 * @brief A Cell class contain a String
 * @inherit Cell
 * 
 * @see Cell
 **/
class Cell_String : public Cell
{
  private:
	std::string contents;

  public:
	/**
	 * @brief Give the type of cell of the object
	 * @return The type of object
	 * 
	 * @see Cell::get_sort
	 */
	sort get_sort() const override;

	/**
	 * @brief Convert the cell's content to string
	 * @return A string representation of the content of the cell
	 * 
	 * @see Cell:to_string
	 */
	std::string to_string() const override;
	
	/**
	 * @brief A constructor for a Cell who contain a string
	 * @param _contents The content to add in the cell
	 * @return A Cell containing the string given
	 **/
	explicit Cell_String(std::string s);
	/**
	 * @brief Give the content of the Cell
	 * @return the text of the string in the Cell
	 **/
	std::string get_contents() const;
};

/*****************************/
/**
 * @class Cell_Symbol
 * @brief A Cell class contain a Symbol
 * @inherit Cell
 * 
 * @see Cell
 **/
class Cell_Symbol : public Cell
{
  private:
	std::string contents;

  public:
	/**
	 * @brief Give the type of cell of the object
	 * @return The type of object
	 * 
	 * @see Cell::get_sort
	 */
	sort get_sort() const override;

	/**
	 * @brief Convert the cell's content to string
	 * @return A string representation of the content of the cell
	 * 
	 * @see Cell:to_string
	 */
	std::string to_string() const override;

	/**
	 * @brief A constructor for a Cell who contain a symbol
	 * @param _contents The content to add in the cell
	 * @return A Cell containing the symbol given
	 **/
	explicit Cell_Symbol(std::string s);
	/**
	 * @brief Give the content of the Cell
	 * @return the text of the symbol in the Cell
	 **/
	std::string get_contents() const;
};

/*****************************/
/**
 * @class Cell_Pair
 * @brief A Cell class contain a Pair
 * @inherit Cell
 * 
 * @see Cell
 **/
class Cell_Pair : public Cell
{
  private:
	Cell *car;
	Cell *cdr;

  public:
	/**
	 * @brief Give the type of cell of the object
	 * @return The type of object
	 * 
	 * @see Cell::get_sort
	 */
	sort get_sort() const override;

	/**
	 * @brief Convert the cell's content to string
	 * @return A string representation of the content of the cell
	 * 
	 * @see Cell:to_string
	 */
	std::string to_string() const override;
	
	/**
	 * @brief A constructor for a Cell who contain a pair of objects
	 * @param _car The first content to add in the cell
	 * @param _cdr The second content to add in the cell
	 * @return A Cell containing the pair
	 **/
	explicit Cell_Pair(Cell *_car, Cell *_cdr);
	/**
	 * @brief Give the first content of the Cell
	 * @return the first object of the Cell
	 **/
	Cell *get_car() const;
	/**
	 * @brief Give the second content of the Cell
	 * @return the second object of the Cell
	 **/
	Cell *get_cdr() const;
};

/*****************************/
/**
 * @class Cell_Subr
 * @brief A Cell class contain a Subroutine
 * @inherit Cell
 * 
 * @see Cell
 **/
class Cell_Subr : public Cell
{
  private:
	std::string subr_name;
	Environment* closure;

  public:
	/**
	 * @brief Give the type of cell of the object
	 * @return The type of object
	 * 
	 * @see Cell::get_sort
	 */
	sort get_sort() const override;

	/**
	 * @brief Convert the cell's content to string
	 * @return A string representation of the content of the cell
	 * 
	 * @see Cell:to_string
	 */
	std::string to_string() const override;

	/**
	 * @brief A constructor for a Cell which contains a subroutine
	 * @param _contents The content to add in the cell
	 * @return A Cell containing the subroutine given
	 **/
	explicit Cell_Subr(std::string s_name);

	/**
	 * @brief A constructor for a Cell which contains a subroutine
	 * @param _contents The content to add in the cell
	 * @param closure The closure of the subroutine, if it is a lambda
	 * @return A Cell containing the subroutine given
	 **/
	explicit Cell_Subr(std::string s_name, Environment* closure);

	/**
	 * @brief Give the content of the Cell
	 * @return the text of the subroutine in the Cell
	 **/
	std::string get_subr_name() const;

	/** 
	 * @brief Give the closure of the subroutine, assuming it is a lambda
	 * @return the closure of the lambda
	 **/
	Environment* get_closure() const;
};