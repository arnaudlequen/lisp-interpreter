#!/bin/bash

#Lecture des dossiers
for i in `ls -d */`
do
	cd $i
	echo "Test of `cat about` : \n"
	#lectures des tests du dossier
	for j in `ls *.in 2> /dev/null`
	do
		j=${j%.in}
		../../../lisp < $j.in > $j.va
	done
	cd ../
done
