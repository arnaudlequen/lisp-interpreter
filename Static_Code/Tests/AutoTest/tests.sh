#!/bin/bash

cd ../../
make
cd Tests/AutoTest/

echo "Press [Enter] to continue..."
read a

pass=0
fail=0

#Lecture des dossiers
for i in `ls -d */`
do
	cd $i
	echo "\nTest of `cat about` : \n"
	#lectures des tests du dossier
	for j in `ls *.in 2> /dev/null`
	do
		j=${j%.in}
		../../../lisp < $j.in > $j.out
		#contrôle de la validité des tests
		resultat=`cmp $j.out $j.va`
		if [ -z "$resultat" ]
		then
			echo "Test `cat about` n°$j : Pass"
			pass=$(($pass + 1))
		else
			echo "Test `cat about` n°$j : Fail"
			fail=$(($fail + 1))
		fi
		rm -f `ls *.out`
	done
	cd ../
done
tot=$(($pass+$fail))
echo "\nTotal : ${tot}\tPass : ${pass}\tFail : ${fail}"
