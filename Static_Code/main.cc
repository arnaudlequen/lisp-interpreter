/**
 * @file main.cc
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contains the entry point
 */

#include <iostream>
#include "object.hh"
#include "read.hh"
#include "environment.hh"
#include "eval.hh"
#include "defs.hh"

//Tests
//#include "Tests/tests.hpp"

using namespace std;

int main()//(int argc, char **argv)
{
  /*if (argc > 1)
  {
    Tests::Test_eval();
    return 0;
  }*/

  Environment *env = new Environment();
  while (true)
  {
    cout << "Lisp? " << flush;
    try
    {
      Object l = read_object();
      //cout << l << endl;

      //Handle directive
      /* 
      if (listp(l) && symbolp(car(l)))
      {
        if (car(l) == lisp_setq())
        {
          cout << "SETQQQ" << endl;
          break;
        }
        else if (car(l) == lisp_define())
        {
          cout << "DEFINEEE" << endl;
          break;
        }
        else if (car(l) == lisp_defun())
        {
          cout << "DEFUNNN" << endl;
          break;
        }
      } */

      //Call the eval function
      cout << eval(l, env) << endl;
    }
    catch (runtime_error e)
    {
      cout << "fin du fichier atteinte !" << endl;

      exit(0);
    }
    catch (string e)
    {
      cout << "List error! " << e << endl;
    }
  }
}
