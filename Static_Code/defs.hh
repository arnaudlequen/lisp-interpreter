/**
 * @file defs.hh
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contain all do_, lisp_ and macro definitions of the interpreter
 */

#include "object.hh"
#include "environment.hh"


/**
 * @brief Eval all the terms of a lisp list
 * @param l The list Object to eval
 * @param env The current environment who will be used to eval items of the list
 * @return The list of evaluated items
 * @ver 1.0
 * @date 22-02-2018
 *
 * This function try to eval a lisp list, and return the result in an object
 **/
Object eval_list(const Object &l, Environment *env);

/**
 * @brief create an eval subroutine
 * @return an eval subroutine
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo
 **/
Object lisp_eval();

/**
 * @brief create an apply subroutine
 * @return an apply subroutine
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_apply();

/**
 * 
 * @brief Apply a function f to args in l
 * @param f the function which is applied
 * @param l The parsed lisp code, which contains a list of args for f
 * @param env The current environment which will be used to eval items of the list
 * @return The result of the application of the item
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo
 **/
Object do_apply(const Object& f, const Object &l, Environment *env);

/**
 * @brief Eval all items of the list, and return the last
 * @param l The list Object to eval
 * @param env The current environment who will be used to eval items of the list
 * @return The evaluation of the last evaluated item
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_progn(Object &l, Environment *env);

/**
 * @brief Apply a if condition
 * @param l The parsed lisp code, who contain a condition, a left value and a right value
 * @param env The current environment who will be used to eval items of the list
 * @return The result of the evaluated item of the condition
 * @ver 1.0
 * @date 22-02-2018
 * 
 * do_if apply a lisp condition 
 **/
Object do_if(Object &l, Environment *env);

/**
 * @brief Apply a if condition
 * @param l The parsed lisp code, who contain a condition, a left value and a right value
 * @param env The current environment who will be used to eval items of the list
 * @return The result of the first successfull evaluated item of the list
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_cond(Object &l, Environment *env);

/**
 * @brief if the eval of the first arg is false, return nil, otherwise apply the rest of the list
 * @param l the list of args to evaluate
 * @param env The current environment who will be used to eval items of the list
 * @return nil if the first arg is valid, the result of the second item otherwise
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_andthen(Object &l, Environment *env);

/**
 * @brief Apply an eval
 * @param l The parsed lisp code, who contain a condition, a left value and a right value
 * @param env The current environment who will be used to eval items of the list
 * @return The result of the evaluation
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo 
 **/
Object do_eval(const Object &l, Environment *env);

/**
 * @brief Apply a if condition
 * @param l The parsed lisp code, who contain a condition, a left value and a right value
 * @param env The current environment who will be used to eval items of the list
 * @return The result of the application of the item
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_apply(Object &l, Environment *env);

// Opérateurs

/**
 * @brief create a plus symbol
 * @return a "+" symbol
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo Corrections needed
 **/
Object lisp_plus();

/**
 * @brief Apply a plus operation
 * @param l the list of args to evaluate
 * @param env The current environment who will be used for the evaluation
 * @return an object who is the result of the operation
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_plus(const Object &l, Environment *env);

/**
 * @brief create a minus symbol
 * @return a "-" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_minus();

/**
 * @brief Apply a minus operation
 * @param l the list of args to evaluate
 * @param env The current environment who will be used for the evaluation
 * @return an object who is the result of the operation
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_minus(const Object &l, Environment *env);

/**
 * @brief create a multiply symbol
 * @return a "*" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_mult();

/**
 * @brief Apply a multiplication operation
 * @param l the list of args to evaluate
 * @param env The current environment who will be used for the evaluation
 * @return an object who is the result of the operation
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_mult(const Object &l, Environment *env);

/**
 * @brief create a division symbol
 * @return a "/" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_div();

/**
 * @brief Apply a division operation
 * @param l the list of args to evaluate
 * @param env The current environment who will be used for the evaluation
 * @return an object who is the result of the operation
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_div(const Object &l, Environment *env);

/**
 * @brief create a concat subroutine
 * @return a concat subroutine
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_concat();

/**
 * @brief Do a concatenation subroutine
 * @param l the list of args to evaluate
 * @param env The current environment who will be used for the evaluation
 * @return an object who is the result of the subroutine
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_concat(const Object &l, Environment *env);

// -----------------

/**
 * @brief create a car subroutine
 * @return a car subroutine
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_car();

/**
 * @brief Do a car subroutine
 * @param l the list of args to evaluate
 * @param env The current environment who will be used for the evaluation
 * @return an object who is the first item of the list
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_car(const Object &l, Environment *env);

/**
 * @brief create a cdr subroutine
 * @return a cdr subroutine
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_cdr();

/**
 * @brief Do a cdr subroutine
 * @param l the list of args to evaluate
 * @param env The current environment who will be used for the evaluation
 * @return an object who contain all items of the list exept the first
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_cdr(const Object &l, Environment *env);

/**
 * @brief create a cons subroutine
 * @return a cons subroutine
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_cons();

/**
 * @brief Do a cons subroutine
 * @param l the list of args to evaluate
 * @param env The current environment who will be used for the evaluation
 * @return an object who is the concatenation of the item and his next
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_cons(const Object &l, Environment *env);

/**
 * @brief create a eq symbol
 * @return an "eq" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_eq();

/**
 * @brief Do a concatenation subroutine
 * @param l the list of args to evaluate
 * @return a bool object who is the result of the comparison between the two objects
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_eq(const Object &l);

/**
 * @brief create a equal symbol
 * @return an "=" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_equal();

/**
 * @brief Do a concatenation subroutine
 * @param l the list of args to evaluate
 * @param env The current environment who will be used for the evaluation
 * @return a bool object who is true if the content of the two objects is identic, false otherwise
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_equal(const Object &l, Environment *env);

/**
 * @brief create a greater than symbol
 * @return a ">" subroutine
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_gt();

/**
 * @brief Do a > symbol
 * @param l the list of args to evaluate
 * @param env The current environment who will be used for the evaluation
 * @return an bool object who is the result of the test
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_gt(const Object &l, Environment *env);

/**
 * @brief create a greather or equal symbol
 * @return a ">=" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_geq();

/**
 * @brief Do a >= symbol
 * @param l the list of args to evaluate
 * @param env The current environment who will be used for the evaluation
 * @return an bool object who is the result of the test
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_geq(const Object &l, Environment *env);

/**
 * @brief create a lether than symbol
 * @return a "<" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_lt();

/**
 * @brief Do a < symbol
 * @param l the list of args to evaluate
 * @param env The current environment who will be used for the evaluation
 * @return an bool object who is the result of the test
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_lt(const Object &l, Environment *env);

/**
 * @brief create a lether or equal than symbol
 * @return a "<=" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_leq();

/**
 * @brief Do a <= symbol
 * @param l the list of args to evaluate
 * @param env The current environment who will be used for the evaluation
 * @return an bool object who is the result of the test
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_leq(const Object &l, Environment *env);

/**
 * @brief create a read subroutine
 * @return a read subroutine
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_read();

/**
 * @brief Apply a read symbol
 * @param l the list of args to evaluate
 * @return an object who is the item given by the user
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo Function who need to be implement
 **/
Object do_read();

/**
 * @brief create a print subroutine
 * @return a print subroutine
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_print();

/**
 * @brief Apply a print subroutine
 * @param l the object to print
 * @return an object who is the item given by the user
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_print(const Object &l, Environment *env);

/**
 * @brief create a newline subroutine
 * @return a newline subroutine
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_newline();

/**
 * @brief Apply a newline subroutine
 * @param l the object to newline
 * @return an object who is the item given by the user
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo to be implement
 **/
Object do_newline(const Object &l);

/**
 * @brief create a end subroutine
 * @return a end subroutine
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo
 **/
Object lisp_end();

/**
 * @brief Apply a end subroutine
 * @param l the object to end
 * @return an object who is the item given by the user
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo Function who need to be implement
 **/
Object do_end();

/**
 * @brief create a lambda subroutine
 * @return an lambda subroutine
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_lambda();

/**
 * @brief Do a lambda subroutine
 * @param l The function params
 * @param lvals the function data
 * @param env The current environment who will be used for the evaluation
 * @return an object who is a new Lisp function
 * @ver 1.0
 * @date 22-02-2018
 **/
Object do_lambda(const Object &f, const Object &lvals, Environment *env);

// keywords

/**
 * @brief Create a lambda subroutine
 * @return An object who is a lambda subroutine
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_lambda();

/**
 * @brief Create a lambda subroutine, giving a closure
 * @return An object who is a lambda subroutine
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_lambda(Environment* closure);

/**
 * @brief Create a quote symbol
 * @return An object who is a "quote" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_quote();

/**
 * @brief Create a setq symbol
 * @return An object who is a "setq" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_setq();


/**
 * @brief Create a define symbol
 * @return An object who is a "define" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_define();

/**
 * @brief Create a defun symbol
 * @return An object who is a "defun" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_defun();

/**
 * TODO
 **/

Object do_defun(const Object &f,const Object& args,const Object &op, Environment *env);

/**
 * @brief Create a if symbol
 * @return An object who is a "if" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_if();

/**
 * @brief Create a cond symbol
 * @return An object who is a "cond" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_cond();

/**
 * @brief Create a andthen symbol
 * @return An object who is a "andthen" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_andthen();

/**
 * @brief Create a progn symbol
 * @return An object who is a "progn" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_progn();

/**
 * @brief Create a printenv symbol
 * @return An object who is a "printenv" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_printenv();

/**
 * @brief print the given environment
 * @param env The environment to print
 * @ver 1.0
 * @date 22-02-2018
 **/
void printenv(Environment *env);

/**
 * @brief Create a debug symbol
 * @return An object who is a "debug" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_debug();

/**
 * @brief Create a load symbol
 * @return An object who is a "load" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_load();

// vérification de la nature des objets

/**
 * @brief Create a nil symbol
 * @return An object who is "nil"
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo need to be done
 **/
Object lisp_null();

/**
 * @brief Create a quote symbol
 * @return An object who is a "quote" symbol
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo need to be done
 **/
bool do_null(const Object &l);

/**
 * @brief Create a stringp subroutine
 * @return An object who is a "stringp" subroutine
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo
 **/
Object lisp_stringp();

/**
 * @brief Do a stringp subroutine
 * @param l the list of args to evaluate
 * @return a bool object, true if the object is a string, false otherwise
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo
 **/
bool do_stringp(const Object &l);

/**
 * @brief Create a numberp subroutine
 * @return An object who is a "numberp" subroutine
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo
 **/
Object lisp_numberp();

/**
 * @brief Do a numberp subroutine
 * @param l the list of args to evaluate
 * @return a bool object, true if the object is a number, false otherwise
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo
 **/
bool do_numberp(const Object &l);

/**
 * @brief Create a symbolp subroutine
 * @return An object who is a "symbolp" subroutine
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo
 **/
Object lisp_symbolp();

/**
 * @brief Do a symbolp subroutine
 * @param l the list of args to evaluate
 * @return a bool object, true if the object is a symbol, false otherwise
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo
 **/
bool do_symbolp(const Object &l);

/**
 * @brief Create a listp subroutine
 * @return An object who is a "listp" subroutine
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo
 **/
Object lisp_listp();

/**
 * @brief Do a listp subroutine
 * @param l the list of args to evaluate
 * @return a bool object, true if the object is a list, false otherwise
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo
 **/
bool do_listp(const Object &l);

//errors
/**
 * @brief Create an error symbol
 * @return An object who is a "error" symbol
 * @ver 1.0
 * @date 22-02-2018
 * 
 * @todo
 **/
Object lisp_error();

//macro sur les listes

/**
 * @brief Create a let symbol
 * @return An object who is a "let" symbol
 * @ver 1.0
 * @date 22-02-2018
 **/
Object lisp_let();

/**
 * @brief Expand the let keyword in Lisp
 * @return The expanded object
 **/
Object macro_let_to_lambda(const Object &l);

// debug (later)

Object lisp_map_car();
Object map_car(const Object& l);