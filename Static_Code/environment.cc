/**
 * @file environment.cc
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contain implementation of Environment class
 */

#include "environment.hh"
#include "defs.hh"
#include <stdexcept>
#include <cassert>
#include <string>

using namespace std;


//Association between symbols and values
Binding::Binding(std::string _name, const Object& _value)
{
    name = _name;
    value = _value;
}

void Binding::set_value(const Object& _value)
{
    value = _value;
}

std::string Binding::get_name()
{
    return name;
}

Object Binding::get_value()
{
    return value;
}


//Items that make a list
Env_Cell::Env_Cell(Binding* _b, Env_Cell* _n)
{
    binding = _b;
    next = _n;
}

Binding* Env_Cell::get_binding()
{
    return binding;
}
Env_Cell* Env_Cell::get_next()
{
    return next;
}



Environment::Environment()
{
    a_list = nullptr;
    count = 0;
}

Environment::Environment(Environment *env)
{
    a_list = env->a_list; //Deep copy unnecessary
    count = env->count;
}

Object Environment::value(Object l)
{
    assert(symbolp(l));
    std::string symbol_value = Object_to_string(l);

    //We check if the symbol is bound
    for (Env_Cell* cell = a_list; cell != nullptr; cell = cell->get_next())
    {
        Binding* binding = cell->get_binding();

        if (binding->get_name() == symbol_value){
            return binding->get_value();
        }
    }
    //If the symbol hasn't been found in the environment, we check if it is a native subroutine
    //TODO: Use an hashtable
    if (symbol_value == "+") return lisp_plus();

    else if (symbol_value == "-") return lisp_minus();

    else if (symbol_value == "*") return lisp_mult();

    else if (symbol_value == "/") return lisp_div();

    else if (symbol_value == "concat") return lisp_concat();

    else if (symbol_value == "car") return lisp_car();

    else if (symbol_value == "cdr") return lisp_cdr();

    else if (symbol_value == "cons") return lisp_cons();

    else if (symbol_value == "lambda") return lisp_lambda(this);

    else if (symbol_value == "=") return lisp_equal();

    else if (symbol_value == ">") return lisp_gt();

    else if (symbol_value == ">=") return lisp_geq();

    else if (symbol_value == "<") return lisp_lt();

    else if (symbol_value == "<=") return lisp_leq();

    throw std::string("Undefined variable: " + symbol_value);
}

void Environment::extend(const Object &symbol, const Object &value)
{
    std::string symbol_name = Object_to_string(symbol);

    extend(symbol_name, value);
}

void Environment::extend(std::string symbol_name, const Object &value)
{
    Binding* new_binding = new Binding(symbol_name, value);
    Env_Cell* new_cell = new Env_Cell(new_binding, a_list);

    a_list = new_cell;

    count += 1;
}



void Environment::modify(const Object &symbol, const Object &value)
{
    std::string symbol_name = Object_to_string(symbol);

    modify(symbol_name, value);
}

void Environment::modify(std::string symbol, const Object &value)
{
    Env_Cell* cell_p = a_list;
    while(cell_p != nullptr)
    {
        Binding* binding = cell_p->get_binding();

        if (binding->get_name() == symbol)
        {
            binding->set_value(value);
            break;
        }

         cell_p = cell_p->get_next();
    }
    if (cell_p == nullptr)
        extend(symbol, value);
}


void Environment::extend_assoc(const Object &symbols, const Object &values)
{
    if (!null(symbols) && !null(values))
    {
        extend(car(symbols), car(values));
        extend_assoc(cdr(symbols), cdr(values));
    }
}

Object Environment::print()
{
    string trc = "";
    for (Env_Cell* cell = a_list; cell != nullptr; cell = cell->get_next())
    {
        Binding* binding = cell->get_binding();

        std::string sym = binding->get_name();
        Object      val = binding->get_value();

        cout << sym << " := " << val << "; ";
    }
    return (string_to_Object(trc));
}

int Environment::size()
{
    return count;
}
