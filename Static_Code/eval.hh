/**
 * @file eval.hh
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contain implementation of the eval function
 */

#ifndef __EVAL_H__
#define __EVAL_H__

class Cell;
using Object = Cell *;
class Environment;

/**
 * @brief This function evaluate the content of the object given, and execute it 
 * @param l The object who need to be evaluate
 * @param env The environment in which we want to work
 * @return The object corresponding to the evaluation of the l object
 **/
Object eval(Object l, Environment *env);

#endif //__EVAL_H__