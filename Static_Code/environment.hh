/**
 * @file environment.hh
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contain definition of Environment class
 */

#ifndef __ENVIRONMENT_H__
#define __ENVIRONMENT_H__

#include "object.hh"
#include "cell.hh"
#include <vector>
#include <string>

class Binding
{  
    public:
        Binding(std::string _name, const Object& _value);

        void set_value(const Object& _value);

        std::string get_name();
        Object get_value();
    private:
        std::string name;
        Object value;
};

class Env_Cell
{
    public:
        Env_Cell(Binding* _b, Env_Cell* _n);

        Binding* get_binding();
        Env_Cell* get_next();

    private:
        Binding* binding;
        Env_Cell* next;
};

class Environment
{
  public:
    Environment();

    Environment(Environment *env);

    /** 
     * @brief Give the object associate to the given symbol
     * @param l The symbol for wich we want to find the associated object
     * @return the value a symbol is bound to.
     * @note If the wanted item doesn't exist, this function will raise a string exception
     **/
    Object value(Object l);

    /**
     * @brief Add an item into the memory
     * @param symbol The symbol associated to the new object
     * @param value The object associated to the symbol
     **/
    void extend(const Object &symbol, const Object &value);

    /**
     * @brief Add an item into the memory
     * @param symbol The name of the symbol associated to the new object
     * @param value The object associated to the new symbol
     **/
    void extend(std::string symbol, const Object &value);

    /**
     * @brief Try to modify the value of the given symbol
     * @param symbol The name of the symbol we want to change the value of
     * @param value The object associated to said symbol
     **/
    void modify(const Object &symbol, const Object &value);

    /**
     * @brief Try to modify the value of the given symbol
     * @param symbol The name of the symbol we want to change the value of
     * @param value The object associated to said symbol
     **/
    void modify(std::string symbol, const Object &value);

    /**
     * @brief Assign a set of objects to a set of symbols
     * @param symbol The name of the symbol associated to the new object
     * @param value The object associated to the symbol
     **/
    void extend_assoc(const Object &symbols, const Object &values);

    /**
     * @brief Generate an object who contains a string of all the environment
     * @return The generatesd object
     **/
    Object print();

    /**
     * @brief Give the size of the environment
     * @return The size of the environment
     **/
    int size();

  private:
    Env_Cell* a_list;
    int count;

};

#endif //__ENVIRONMENT_H__