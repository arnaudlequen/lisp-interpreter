/**
 * @file cell.cc
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contains implementation of Cell classes
 **/

#include <iostream>
#include "cell.hh"
#include "environment.hh"

using namespace std;

static void trace(string s) {
  //clog << s;
}

/*****************************/

Cell_Number::Cell_Number(int _contents):
  contents(_contents) {
  trace("n");
}

Cell::sort Cell_Number::get_sort() const {
  return sort::NUMBER;
}

string Cell_Number::to_string() const {
  	return (std::to_string(contents));
}

int Cell_Number::get_contents() const {
  return contents;
}

/*****************************/

Cell_String::Cell_String(string _contents):
  contents(_contents) {
  trace("t");
}

Cell::sort Cell_String::get_sort() const {
  return sort::STRING;
}

string Cell_String::to_string() const {
  	return contents;
}

string Cell_String::get_contents() const {
  return contents;
}

/*****************************/

Cell_Symbol::Cell_Symbol(string _contents):
  contents(_contents) {
  trace("s");
}

Cell::sort Cell_Symbol::get_sort() const {
  return sort::SYMBOL;
}

string Cell_Symbol::get_contents() const {
  return contents;
}

string Cell_Symbol::to_string() const {
  	return contents;
}

/*****************************/

Cell_Pair::Cell_Pair(Cell *_car, Cell *_cdr):
  car(_car), cdr(_cdr) {
  trace("p");
}

Cell::sort Cell_Pair::get_sort() const {
  return sort::PAIR;
}

string Cell_Pair::to_string() const {
  	return "(...)";
}

Cell *Cell_Pair::get_car() const {
  return car;
}

Cell *Cell_Pair::get_cdr() const {
  return cdr;
}


/*****************************/

Cell_Subr::Cell_Subr(std::string s_name):
  subr_name(s_name){
    closure = nullptr;
  trace("r");
}

Cell_Subr::Cell_Subr(std::string s_name, Environment* _closure):
  subr_name(s_name){
    closure = new Environment(_closure);
  trace("r");
}

Cell::sort Cell_Subr::get_sort() const {
  return sort::SUBR;
}

string Cell_Subr::to_string() const {
  	return subr_name;
}

string Cell_Subr::get_subr_name() const {
  return subr_name;
}

Environment* Cell_Subr::get_closure() const {
  return closure;
}