/**
 * @file apply.cc
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contains implementation of apply and apply_subr functions
 */

#include "apply.hh"
#include "object.hh"
#include "environment.hh"
#include "eval.hh"
#include "defs.hh"
#include <cassert>

using namespace std;

/**
 * @brief Apply a function to his arguments, if the item is a subroutine
 * @param f The function to apply
 * @param lvals The parameters of the function
 * @param env The environment needed to execute the function
 * @return The object in wich the function was evaluated
 **/
Object apply_subr(const Object &f, const Object &lvals, Environment *env)
{
    assert(subrp(f));
    Cell_Subr *subr = dynamic_cast<Cell_Subr *>(f);
    std::string subr_name = subr->get_subr_name();

    if (subr_name == "+")
    {
        return do_plus(lvals, env);
    }
    else if (subr_name == "-")
    {
        return do_minus(lvals, env);
    }
    else if (subr_name == "*")
    {
        return do_mult(lvals, env);
    }
    else if (subr_name == "/")
    {
        return do_div(lvals, env);
    }
    else if (subr_name == "concat")
    {
        return do_concat(lvals, env);
    }
    else if (subr_name == "car")
    {
        return do_car(lvals, env);
    }
    else if (subr_name == "cdr")
    {
        return do_cdr(lvals, env);
    }
    else if (subr_name == "cons")
    {
        return do_cons(lvals, env);
    }
    else if (subr_name == "lambda")
    {
        return do_lambda(f, lvals, env);
    }
    else if (subr_name == "=")
    {
        return do_equal(lvals, env);
    }
    else if (subr_name == ">")
    {
        return do_gt(lvals, env);
    }
    else if (subr_name == ">=")
    {
        return do_geq(lvals, env);
    }
    else if (subr_name == "<")
    {
        return do_lt(lvals, env);
    }
    else if (subr_name == "<=")
    {
        return do_leq(lvals, env);
    }

    return nil();
}


Object apply(const Object &f, const Object &lvals, Environment *env)
{

    if (null(f))
    {
        throw string("Cannot apply nil");
        return nil();
    }
    if (numberp(f))
    {
        throw string("Cannot apply a number : " + to_string(Object_to_number(f)));
        return nil();
    }
    if (stringp(f))
    {
        throw string("Cannot apply a string : " + Object_to_string(f));
        return nil();
    }
    if (subrp(f))
    {
        return apply_subr(f, lvals, env);
    }
    if (symbolp(f))
    {
        Object new_f = eval(f, env);
        return apply(new_f, lvals, env);
    }
    if (Object_to_string(car(f)) == "lambda") //It's a lambda
    {
        return do_lambda(f, lvals, env);
    }
    else //CLEAN THIS
    {
        throw string("Cannot apply a list : " + Object_to_string(f));
    }

    //Should never happen
    return nil();
}