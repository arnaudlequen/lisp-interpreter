/**
 * @file read.cc
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contains some reading functions
 */
#include <iostream>
#include <stdexcept>

#include "object.hh"
#include "read.hh"

using namespace std;

extern Object get_read_Object();
extern int yyparse();
extern void yyrestart(FILE *fh);

Object read_object()
{
  if (yyparse() != 0)
    throw runtime_error("End of input stream");
  Object l = get_read_Object();
  // cout << "Read: " << l << endl;
  return l;
}

void read_object_restart(FILE *fh)
{
  yyrestart(fh);
}
