;Fibonacci
(setq fib (lambda (n) (cond ((= n 0) 1) ((= n 1) 1) (t (+ (fib (- n 1)) (fib (- n 2)))))))

(fib 0)
(fib 1)
(fib 2)
(fib 5)

;Need to generate an error
(fib )

(end)
