#!/bin/bash

echo "Appuyez sur [Entrée] pour continuer..."
read a

clear
echo "Test de setq et opérateurs : \n"
cat TSetq.lsp
echo "\n"
../lisp < TSetq.lsp

echo "Appuyez sur [Entrée] pour continuer..."
read a
clear
echo "Test de lambda : \n"
cat TLambda.lsp
echo "\n"
../lisp < TLambda.lsp

echo "Appuyez sur [Entrée] pour continuer..."
read a
clear
echo "Test des listes : \n"
cat TList.lsp
echo "\n"
../lisp < TList.lsp

echo "appuyer sur [Entrée] pour continuer..."
read a
clear
echo "Test de eval : \n"
cat TEval.lsp
echo "\n"
../lisp < TEval.lsp

echo "appuyer sur [Entrée] pour continuer..."
read a
clear
echo "Test de apply : \n"
cat TApply1.lsp
echo "\n"
../lisp < TApply1.lsp
echo "appuyer sur [Entrée] pour continuer..."
read a
cat TApply2.lsp
echo "\n"
../lisp < TApply2.lsp

echo "appuyer sur Entrée pour continuer"
read a
clear
echo "Tests of some functions : \n"
cat TSum.lsp
echo "\n"
../lisp < TSum.lsp

echo "appuyer sur Entrée pour continuer"
read a
clear
cat TLet.lsp
echo "\n"
../lisp < TLet.lsp

echo "appuyer sur Entrée pour continuer"
read a
clear
cat TFact.lsp
echo "\n"
../lisp < TFact.lsp

echo "appuyer sur Entrée pour continuer"
read a
clear
cat TFib.lsp
echo "\n"
../lisp < TFib.lsp

echo "appuyer sur Entrée pour continuer"
read a
clear
cat TMap.lsp
echo "\n"
../lisp < TMap.lsp
