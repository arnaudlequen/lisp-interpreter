(setq f (lambda (m n) (+ m n)))
(f 2 1)
(setq f (lambda (m n) (* m n)))
(f 2 1)
(end)
