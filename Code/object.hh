/**
 * @file object.hh
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contain definition of the Object functions
 */
#pragma once

#include <iostream>
#include <string>

class Cell;
using Object = Cell *;

/**
 * @brief create a nil Object
 * @return a nil Object
 * @version 1.0
 * @date 22-02-2018
 * 
 * @see null
 * @see bool_to_Object
 **/
Object nil();

/**
 * @brief create a nil Object
 * @param l The Object we wat to test
 * @return t if the given object is nil, nil otherwise
 * @version 1.0
 * @date 22-02-2018
 * 
 * @see nil
 * @see t
 * @see bool_to_Object
 **/
bool null(const Object &l);

/**
 * @brief create a t Object
 * @return a t Object
 * @version 1.0
 * @date 22-02-2018
 * 
 * @see null
 * @see bool_to_Object
 **/
Object t();

/**
 * @brief Test if the two given Objects are identical
 * @param a The first Object to compare
 * @param b The second Object to compare
 * @return true if the twice Objects are similar, false otherwise
 **/
bool eq(const Object &a, const Object &b);

/**
 * @brief convert a number into an Object
 * @param n The number to convert
 * @return The created Object
 * 
 * @see Object_to_number
 * @see string_to_Object
 * @see symbol_to_Object
 * @see bool_to_Object
 * @see subr_to_Object
 **/
Object number_to_Object(int n);

/**
 * @brief convert a string into an Object
 * @param s The string to convert
 * @return The created Object
 * 
 * @see Object_to_string
 * @see number_to_Object
 * @see symbol_to_Object
 * @see bool_to_Object
 * @see subr_to_Object
 **/
Object string_to_Object(std::string s);

/**
 * @brief convert a symbol into an Object
 * @param s The symbol to convert
 * @return The created Object
 * 
 * @see Object_to_string
 * @see number_to_Object
 * @see string_to_Object
 * @see bool_to_Object
 * @see subr_to_Object
 **/
Object symbol_to_Object(std::string s);

/**
 * @brief convert a bool into an Object
 * @param b The bool to convert
 * @return The created Object
 * 
 * @see null
 * @see number_to_Object
 * @see string_to_Object
 * @see symbol_to_Object
 * @see subr_to_Object
 **/
Object bool_to_Object(bool b);

/**
 * @brief convert a subroutine into an Object
 * @param s_name The subroutine to convert
 * @return The created Object
 * 
 * @see Object_to_string
 * @see number_to_Object
 * @see string_to_Object
 * @see symbol_to_Object
 * @see bool_to_Object
 **/
Object subr_to_Object(std::string s_name);

/**
 * @brief Convert an Object to a number
 * @param l The Object to convert
 * @return The number contained in the Object
 * 
 * @see number_to_Object
 **/
int Object_to_number(const Object &l);

/**
 * @brief Convert an Object to a string
 * @param l The Object to convert
 * @return The string contained in the Object
 * 
 * @see string_to_Object
 * @see symbol_to_Object
 * @see subr_to_Object
 **/
std::string Object_to_string(const Object &l);

/**
 * @brief Control if the given Object is a number
 * @param l The Object we need to test
 * @return true if the given Object is a number, false otherwise
 **/
bool numberp(const Object &l);

/**
 * @brief Control if the given Object is a string
 * @param l The Object we need to test
 * @return true if the given Object is a string, false otherwise
 **/
bool stringp(const Object &l);

/**
 * @brief Control if the given Object is a symbol
 * @param l The Object we need to test
 * @return true if the given Object is a symbol, false otherwise
 **/
bool symbolp(const Object &l);

/**
 * @brief Control if the given Object is a list
 * @param l The Object we need to test
 * @return true if the given Object is a list, false otherwise
 **/
bool listp(const Object &l);

/**
 * @brief Control if the given Object is a subroutine
 * @param l The Object we need to test
 * @return true if the given Object is a subroutine, false otherwise
 **/
bool subrp(const Object &l);

std::ostream &operator<<(std::ostream &s, const Object &l);

/**
 * @brief Add an item into the list
 * @param a The Object to add to the list
 * @param l The existing list in wich we want to add an item
 * @return The new list which contains items of l and a
 **/
Object cons(const Object &a, const Object &l);

/**
 * @brief Give the first item of a list
 * @param l The list for which we want an item
 * @return The first item of the list
 * 
 * @see cadr
 * @see caddr
 * @see cadddr
 **/
Object car(const Object &l);

/**
 * @brief Give all items of the list, except the first one
 * @param l The list for which we want items
 * @return all items of the list, except the first
 * 
 * @see cddr
 * @see cdddr
 **/
Object cdr(const Object &l);

/**
 * @brief Give the second item of a list
 * @param l The list for which we want an item
 * @return The second item of the list
 * 
 * @see car
 * @see caddr
 * @see cadddr
 **/
Object cadr(const Object &l);

/**
 * @brief Give all items of the list, except the first two elements
 * @param l The list for which we want items
 * @return all items of the list, except the first two elements
 * 
 * @see cdr
 * @see cdddr
 **/
Object cddr(const Object &l);

/**
 * @brief Give the third item of a list
 * @param l The list for which we want an item
 * @return The third item of the list
 * 
 * @see car
 * @see cadr
 * @see cadddr
 **/
Object caddr(const Object &l);

/**
 * @brief Give all items of the list, except the first three elements
 * @param l The list for which we want items
 * @return all items of the list, except the first three elements
 * 
 * @see cdr
 * @see cddr
 **/
Object cdddr(const Object &l);

/**
 * @brief Give the fourth item of a list
 * @param l The list for which we want an item
 * @return The fourth item of the list
 * 
 * @see car
 * @see cadr
 * @see caddr
 **/
Object cadddr(const Object &l);

class Binding
{  
    public:
        Binding(std::string _name, const Object& _value);

        std::string get_name();
        Object get_value();
    private:
        std::string name;
        Object value;
};

Binding* create_binding(std::string symbol_name, const Object &value);

std::string Binding_to_name(Binding* b);

Object Binding_to_Object(Binding* b);

class Env_Cell
{
    public:
        Env_Cell(Binding* _b, Env_Cell* _n);

        Binding* get_binding();
        Env_Cell* get_next();

    private:
        Binding* binding;
        Env_Cell* next;
};

Env_Cell* create_env_cell (Binding* b, Env_Cell* a_list);

Binding* Object_to_binding(Env_Cell* p);

Env_Cell* next_cell (Env_Cell* p);

