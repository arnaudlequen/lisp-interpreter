/**
 * @file object.cc
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contain implementation of the Object functions
 */

#include <string>
#include <iostream>
#include <cassert>
#include <sstream>

#include "object.hh"
#include "cell.hh"
#include "environment.hh"

using namespace std;

Object nil()
{
	static Cell *c = new Cell_Symbol("nil");
	return c;
}

bool null(const Object &l)
{
	return l == nil();
}

Object t()
{
	static Cell *c = new Cell_Symbol("t");
	return c;
}

Object cons(const Object &a, const Object &l)
{
	return new Cell_Pair(a, l);
}

Object car(const Object &l)
{
	if (!listp(l))
		throw string("Cannot take car of such an object : " + 
					Object_to_string(l));

	return (dynamic_cast<Cell_Pair *>(l))->get_car();
}

Object cdr(const Object &l)
{
	if (!listp(l))
		throw string("Cannot take cdr of such an object : " + 
					Object_to_string(l));

	return (dynamic_cast<Cell_Pair *>(l))->get_cdr();
}

Object number_to_Object(int n)
{
	return new Cell_Number(n);
}

Object string_to_Object(string s)
{
	return new Cell_String(s);
}

Object symbol_to_Object(string s)
{
	return new Cell_Symbol(s);
}

Object bool_to_Object(bool b)
{
	if (b)
		return t();
	return nil();
}

Object subr_to_Object(std::string s_name)
{
	return new Cell_Subr(s_name);
}

int Object_to_number(const Object &l)
{
	assert(numberp(l));
	return (dynamic_cast<Cell_Number *>(l))->get_contents();
}

static void print_aux(string* s, const Object &l)
{
	assert(listp(l));
	if (null(l))
	{
		*s += Object_to_string(nil());
	}
	else
	{
		*s += Object_to_string(car(l));
		if (!null(cdr(l)))
		{
			*s += " ";
			print_aux(s, cdr(l));
		}	
	}
	
}

string Object_to_string(const Object &l)
{
	assert(stringp(l) || symbolp(l) || subrp(l) || listp(l) || 
			numberp(l) || null(l));
	if (null(l))
		return "()";
	if (stringp(l))
		return (dynamic_cast<Cell_String *>(l))->get_contents();
	if (symbolp(l))
		return (dynamic_cast<Cell_Symbol *>(l))->get_contents();
	if (subrp(l))
		return (dynamic_cast<Cell_Subr *>(l))->get_subr_name();
	if (numberp(l))
		return (to_string(Object_to_number(l)));
	if (listp(l)){
		string s = "(";
		print_aux(&s, l);
		s += ")";
		return s;
	} 
	

	assert(false);
  return "";
}

bool numberp(const Object &l)
{
	assert(l != nullptr);
	return (l->get_sort() == Cell::sort::NUMBER);
}

bool stringp(const Object &l)
{
	assert(l != nullptr);
	return (l->get_sort() == Cell::sort::STRING);
}

bool symbolp(const Object &l)
{
	assert(l != nullptr);
	return (l->get_sort() == Cell::sort::SYMBOL);
}

bool listp(const Object &l)
{
	assert(l != nullptr);
	return (l->get_sort() == Cell::sort::PAIR);
}

bool subrp(const Object &l)
{
	assert(l != nullptr);
	return (l->get_sort() == Cell::sort::SUBR);
}

ostream &operator<<(ostream &s, const Object &l)
{
	if (null(l))
		return s << Object_to_string(l);
	if (numberp(l))
		return s << Object_to_number(l);
	if (stringp(l))
		return s << Object_to_string(l);
	if (symbolp(l))
		return s << Object_to_string(l);
	if (subrp(l))
		return s << Object_to_string(l);
	if (listp(l))
		return s << Object_to_string(l);

	return s;
}

Object cadr(const Object &l)
{
	return car(cdr(l));
}
Object cddr(const Object &l)
{
	return cdr(cdr(l));
}
Object caddr(const Object &l)
{
	return car(cddr(l));
}
Object cdddr(const Object &l)
{
	return cdr(cddr(l));
}
Object cadddr(const Object &l)
{
	return car(cdddr(l));
}

bool eq(const Object &a, const Object &b)
{
	assert(a != nullptr);
	assert(b != nullptr);
	if (a->get_sort() != b->get_sort())
		return false;
	if (numberp(a))
		return Object_to_number(a) == Object_to_number(b);
	if (stringp(a) || symbolp(a) || subrp(a))
		return Object_to_string(a) == Object_to_string(b);
	return a == b;
}


//Association between symbols and values
Binding::Binding(std::string _name, const Object& _value)
{
    name = _name;
    value = _value;
}

std::string Binding::get_name()
{
    return name;
}

Object Binding::get_value()
{
    return value;
}

std::string Binding_to_name(Binding* b)
{
	return b->get_name();
}

Object Binding_to_Object(Binding* b)
{
	return b->get_value();
}

Binding* create_binding(std::string symbol_name, const Object &value)
{
	return (new Binding(symbol_name, value));
}

//Items that make a list
Env_Cell::Env_Cell(Binding* _b, Env_Cell* _n)
{
    binding = _b;
    next = _n;
}

Binding* Env_Cell::get_binding()
{
    return binding;
}
Env_Cell* Env_Cell::get_next()
{
    return next;
}

Binding* Object_to_binding(Env_Cell* p)
{
	return p->get_binding();
}

Env_Cell* next_cell (Env_Cell* p)
{
	return p->get_next();
}

Env_Cell* create_env_cell (Binding* b, Env_Cell* a_list)
{
	return (new Env_Cell(b,a_list));
}
