/**
 * @file read.hh
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contains some parsing function definitions
 */

#pragma once

#include "object.hh"

/**
 * @brief Read and parse a string given into the interpreter
 * @return An object containing some parsed code
 **/
Object read_object();

/**
 * @brief Reset, read and parse a file given in parameter
 * @param fh The file we want to parse
 * @return An object which contains the parsing of the file
 **/
void read_object_restart(FILE* fh);
