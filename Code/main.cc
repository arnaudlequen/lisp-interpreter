/**
 * @file main.cc
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contains the entry point
 */

#include <iostream>
#include "object.hh"
#include "read.hh"
#include "environment.hh"
#include "eval.hh"
#include "defs.hh"

using namespace std;

int main()
{
  Environment *env = new Environment();
  while (true)
  {
    cout << "Lisp? " << flush;
    try
    {
		//Parse the input
		Object l = read_object();
      
		//Call the eval function
		cout << eval(l, env) << endl;
    }
    catch (runtime_error e)
    {
		cout << "fin du fichier atteinte !" << endl;

		exit(0);
    }
    catch (string e)
    {
		cout << "List error! " << e << endl;
    }
  }
}
