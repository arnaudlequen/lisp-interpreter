/**
 * @file eval.cc
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contain implementation of the eval function
 */

#include <iostream>
#include "object.hh"
#include "read.hh"
#include "environment.hh"
#include "defs.hh"
#include "apply.hh"

using namespace std;

Object eval(Object l, Environment *env)
{
    if (null(l))
    {
        return l;
    }
    else if (eq(l, t()))
    {
        return l;
    }
    else if (stringp(l))
    {
        return l;
    }
    else if (numberp(l))
    {
        return l;
    }
    else if (symbolp(l))
    {
        try
        {
            return env->value(l);
        }
        catch (runtime_error e)
        {
            throw string("Undefined symbol!");
        }
    }
    // We assume the object is a list
    else
    {
        if (symbolp(car(l)) && Object_to_string(car(l))=="lambda")
        {
            return l;
        }
        else if (eq(car(l), lisp_printenv()))
        {
            printenv(env);
            return (nil());
        }
        else if (eq(car(l), lisp_print()))
        {
            cout << do_print(cdr(l), env) << endl;
            return do_print(cdr(l), env);
        }

        else if (eq(car(l), lisp_newline()))
        {
            return (do_newline());
        }

        else if (eq(car(l), lisp_read()))
        {
            return do_read();
        }
        else if (eq(car(l), lisp_quote()))
        {
            return cadr(l);
        }
        else if (eq(car(l), lisp_if()))
        {
            Object o = cdr(l);
            return do_if(o, env);
        }
        else if (eq(car(l), lisp_cond()))
        {
            Object o = cdr(l);
            return do_cond(o, env);
        }
        else if (eq(car(l), lisp_andthen()))
        {
            Object o = cdr(l);
            return do_andthen(o, env);
        }
        else if (eq(car(l), lisp_progn()))
        {
            Object o = cdr(l);
            return do_progn(o, env);
        }

        else if (eq(car(l), lisp_eval()))
        {
            Object o = cadr(l);
            return do_eval(o, env);
        }

        else if (eq(car(l), lisp_apply()))
        {
            Object f = cadr(l);
            Object arg = eval(caddr(l), env);
            return do_apply(f, arg, env);
        }

        else if (eq(car(l), lisp_defun()))
        {
            Object f = cadr(l);
            Object args = caddr(l);
            Object op = cadddr(l);
            return do_defun(f, args, op, env);
        }

        else if (eq(car(l), lisp_map_car()))
        {
            return (map_car(cdr(l)));
        }

        else if (eq(car(l), lisp_let()))
        {
            return (macro_let_to_lambda(l, env));
        }

        else if (eq(car(l), lisp_map()))
        {
            return (do_map(cadr(l), eval(caddr(l), env), env));
        }

        else if (eq(car(l), lisp_setq()))
        {
            env->extend(cadr(l), eval(caddr(l), env));
            cout << "SET: " << Object_to_string(cadr(l)) << " = " 
				 << eval(cadr(l), env) << endl;
            return string_to_Object("");
        }
        else if (eq(car(l), lisp_null()))
        {
            return (do_null(cadr(l)));
        }
        else if (eq(car(l), lisp_stringp()))
        {
            return (do_stringp(cadr(l)));
        }
        else if (eq(car(l), lisp_symbolp()))
        {
            return (do_symbolp(cadr(l)));
        }
        else if (eq(car(l), lisp_numberp()))
        {
            return (do_numberp(cadr(l)));
        }
        else if (eq(car(l), lisp_listp()))
        {
            return (do_listp(cadr(l)));
        }
        else if (eq(car(l), lisp_end()))
        {
            return (do_end());
        }

        else
        {
            return apply(car(l), eval_list(cdr(l), env), env);
        }
    }
}
