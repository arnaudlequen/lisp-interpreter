/**
 * @file defs.hh
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contain all do_, lisp_ and macro definitions of the interpreter
 */

#include "object.hh"
#include "environment.hh"


/**
 * @brief Eval all the terms of a lisp list
 * @param l The list Object to eval
 * @param env The current environment which will be used to eval items of the list
 * @return The list of evaluated items
 * @version 1.0
 * @date 22-02-2018
 *
 * This function's aim is to eval a Lisp list, and return the result in an object
 **/
Object eval_list(const Object &l, Environment *env);

/**
 * @brief create an eval subroutine
 * @return an eval subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_eval();

/**
 * @brief create an apply subroutine
 * @return an apply subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_apply();

/**
 * 
 * @brief Apply a function to the given arguments
 * @param f the function that is applied
 * @param l The parsed Lisp code, which should represent a list of args for f
 * @param env The current environment which will be used to eval items of the list
 * @return The result of the application of the item
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_apply(const Object& f, const Object &l, Environment *env);

/**
 * @brief Eval every item of the list, and return the last
 * @param l The list Object to eval
 * @param env The current environment that will be used to eval the items of the list
 * @return The evaluation of the last evaluated item
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_progn(Object &l, Environment *env);

/**
 * @brief Evaluate a list of Lisp expressions
 * @param l The parsed lisp code, that stores a condition, a left value and a right value
 * @param env The current environment that will be used to eval the items of the list
 * @return The result of the evaluated item of the condition
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_if(Object &l, Environment *env);

/**
 * @brief Apply a if condition
 * @param l The parsed lisp code, that stores a condition, a left value and a right value
 * @param env The current environment that will be used to eval the items of the list
 * @return The result of the first successfuly evaluated item of the list
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_cond(Object &l, Environment *env);

/**
 * @brief if the eval of the first arg is false, return nil, otherwise apply the rest of the list
 * @param l the list of args to evaluate
 * @param env The current environment that will be used to evaluate items of the list
 * @return nil if the first arg is valid, the result of the second item otherwise
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_andthen(Object &l, Environment *env);

/**
 * @brief Apply an eval
 * @param l The parsed lisp code, which contain a condition, a left value and a right value
 * @param env The current environment which will be used to eval items of the list
 * @return The result of the evaluation
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_eval(const Object &l, Environment *env);

/**
 * @brief Eval a Lisp expression
 * @param l The parsed lisp code, that contains a condition, a left value and a right value
 * @param env The current environment that will be used to eval items of the list
 * @return The result of the evaluation of the item
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_apply(Object &l, Environment *env);

// Opérateurs

/**
 * @brief create a plus symbol
 * @return a "+" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_plus();

/**
 * @brief Apply a plus operation
 * @param l the list of args to evaluate
 * @param env The current environment which will be used for the evaluation
 * @return an object which is the result of the operation
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_plus(const Object &l, Environment *env);

/**
 * @brief create a minus symbol
 * @return a "-" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_minus();

/**
 * @brief Apply a minus operation
 * @param l the list of args to evaluate
 * @param env The current environment which will be used for the evaluation
 * @return an object which is the result of the operation
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_minus(const Object &l, Environment *env);

/**
 * @brief create a multiply symbol
 * @return a "*" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_mult();

/**
 * @brief Apply a multiplication operation
 * @param l the list of args to evaluate
 * @param env The current environment which will be used for the evaluation
 * @return an object which is the result of the operation
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_mult(const Object &l, Environment *env);

/**
 * @brief create a division symbol
 * @return a "/" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_div();

/**
 * @brief Apply a division operation
 * @param l the list of args to evaluate
 * @param env The current environment which will be used for the evaluation
 * @return an object which is the result of the operation
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_div(const Object &l, Environment *env);

/**
 * @brief create a concat subroutine
 * @return a concat subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_concat();

/**
 * @brief Create a concatenation subroutine
 * @param l the list of args to evaluate
 * @param env The current environment that will be used for the evaluation
 * @return an object which is the result of the subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_concat(const Object &l, Environment *env);

// -----------------

/**
 * @brief create a car subroutine
 * @return a car subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_car();

/**
 * @brief Do a car subroutine
 * @param l the list of args to evaluate
 * @param env The current environment which will be used for the evaluation
 * @return an object which is the first item of the list
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_car(const Object &l, Environment *env);

/**
 * @brief create a cdr subroutine
 * @return a cdr subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_cdr();

/**
 * @brief Do a cdr subroutine
 * @param l the list of args to evaluate
 * @param env The current environment which will be used for the evaluation
 * @return an object which contain all items of the list exept the first
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_cdr(const Object &l, Environment *env);

/**
 * @brief create a cons subroutine
 * @return a cons subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_cons();

/**
 * @brief Do a cons subroutine
 * @param l the list of args to evaluate
 * @param env The current environment which will be used for the evaluation
 * @return an object which is the concatenation of the item and his next
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_cons(const Object &l, Environment *env);

/**
 * @brief create an eq symbol
 * @return an "eq" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_eq();

/**
 * @brief Create an eq subroutine
 * @param l the list of args to evaluate
 * @return a bool object which is the result of the comparison between the two objects
 * @version 1.0
 * @date 22-02-2018
 **/

Object do_eq(const Object &l);

/**
 * @brief create a equal symbol
 * @return an "=" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_equal();

/**
 * @brief Do a concatenation subroutine
 * @param l the list of args to evaluate
 * @param env The current environment which will be used for the evaluation
 * @return a bool object which is true if the content of the two objects is identic, false otherwise
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_equal(const Object &l, Environment *env);

/**
 * @brief create a greater than symbol
 * @return a ">" subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_gt();

/**
 * @brief Create a > symbol
 * @param l the list of args to evaluate
 * @param env The current environment that will be used for the evaluation
 * @return an bool object which is the result of the test
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_gt(const Object &l, Environment *env);

/**
 * @brief create a greather or equal symbol
 * @return a ">=" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_geq();

/**
 * @brief Do a >= symbol
 * @param l the list of args to evaluate
 * @param env The current environment which will be used for the evaluation
 * @return an bool object which is the result of the test
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_geq(const Object &l, Environment *env);

/**
 * @brief create a lether than symbol
 * @return a "<" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_lt();

/**
 * @brief Do a < symbol
 * @param l the list of args to evaluate
 * @param env The current environment which will be used for the evaluation
 * @return an bool object which is the result of the test
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_lt(const Object &l, Environment *env);

/**
 * @brief create a lether or equal than symbol
 * @return a "<=" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_leq();

/**
 * @brief Do a <= symbol
 * @param l the list of args to evaluate
 * @param env The current environment which will be used for the evaluation
 * @return an bool object which is the result of the test
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_leq(const Object &l, Environment *env);

/**
 * @brief create a read subroutine
 * @return a read subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_read();

/**
 * @brief Apply a read symbol
 * @return an object which is the item given by the user
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_read();

/**
 * @brief create a print subroutine
 * @return a print subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_print();

/**
 * @brief Apply a print subroutine
 * @param l the object to print
 * @param env the current environment
 * @return an object which is the item given by the user
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_print(const Object &l, Environment *env);

/**
 * @brief create a newline subroutine
 * @return a newline subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_newline();

/**
 * @brief Apply a newline subroutine
 * @param l the object to newline
 * @return an object which is the item given by the user
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_newline();

/**
 * @brief create a end subroutine
 * @return a end subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_end();

/**
 * @brief Apply a end subroutine
 * @param l the object to end
 * @return an object which is the item given by the user
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_end();

/**
 * @brief create a lambda subroutine
 * @return an lambda subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_lambda();

/**
 * @brief Do a lambda subroutine
 * @param f The function params
 * @param lvals the function data
 * @param env The current environment which will be used for the evaluation
 * @return an object which is a new Lisp function
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_lambda(const Object &f, const Object &lvals, Environment *env);

// keywords

/**
 * @brief Create a lambda subroutine
 * @return An object which is a lambda subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_lambda();

/**
 * @brief Create a quote symbol
 * @return An object which is a "quote" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_quote();

/**
 * @brief Create a setq symbol
 * @return An object which is a "setq" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_setq();

/**
 * @brief Create a define symbol
 * @return An object which is a "define" symbol
 * @version 1.0
 * @date 22-02-2018
 * 
 * Pas dans l'interpreteur de Luc
 **/
Object lisp_define();

/**
 * @brief Create a defun symbol
 * @return An object which is a "defun" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_defun();

/**
 * TODO
 **/

Object do_defun(const Object &f,const Object& args,const Object &op, Environment *env);

/**
 * @brief Create a if symbol
 * @return An object which is a "if" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_if();

/**
 * @brief Create a cond symbol
 * @return An object which is a "cond" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_cond();

/**
 * @brief Create a andthen symbol
 * @return An object which is a "andthen" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_andthen();

/**
 * @brief Create a progn symbol
 * @return An object which is a "progn" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_progn();

/**
 * @brief Create a printenv symbol
 * @return An object which is a "printenv" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_printenv();

/**
 * @brief print the given environment
 * @param env The environment to print
 * @version 1.0
 * @date 22-02-2018
 **/
void printenv(Environment *env);

/**
 * @brief Create a debug symbol
 * @return An object which is a "debug" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_debug();

/**
 * @brief Create a load symbol
 * @return An object which is a "load" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_load();

// vérification de la nature des objets

/**
 * @brief Create a nil symbol
 * @return An object which is "nil"
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_null();

/**
 * @brief verify if the object is null or not
 * @param l the object to evaluate
 * @return a bool object, t if the object is a string, nil otherwise
 * @version 1.0
 * @date 22-02-2018
**/

Object do_null(const Object &l);

/**
 * @brief Create a stringp subroutine
 * @return An object which is a "stringp" subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_stringp();

/**
 * @brief Do a stringp subroutine
 * @param l the object of args to evaluate
 * @return a bool object, t if the object is a string, nil otherwise
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_stringp(const Object &l);

/**
 * @brief Create a numberp subroutine
 * @return An object which is a "numberp" subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_numberp();

/**
 * @brief Do a numberp subroutine
 * @param l the object to evaluate
 * @return a bool object, true if the object is a number, false otherwise
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_numberp(const Object &l);

/**
 * @brief Create a symbolp subroutine
 * @return An object which is a "symbolp" subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_symbolp();

/**
 * @brief Do a symbolp subroutine
 * @param l the object to evaluate
 * @return a bool object, t if the object is a symbol, nil otherwise
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_symbolp(const Object &l);

/**
 * @brief Create a listp subroutine
 * @return An object which is a "listp" subroutine
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_listp();

/**
 * @brief Do a listp subroutine
 * @param l the object to evaluate
 * @return a bool object, t if the object is a list, nil otherwise
 * @version 1.0
 * @date 22-02-2018
 **/
Object do_listp(const Object &l);

//macro sur les listes

/**
 * @brief Create a let symbol
 * @return An object which is a "let" symbol
 * @version 1.0
 * @date 22-02-2018
 **/
Object lisp_let();

/**
 * @brief Expand the let keyword in Lisp
 * @return the evaluated expanded object
 **/
Object macro_let_to_lambda(const Object &l, Environment *env);

/**
 * @brief Create a map_car symbol
 * @return An object which is a "map_car" symbol
 **/

Object lisp_map_car();

/**
 * @brief Create a list which conatains each first element of 
 * a list in the list given in argument
 * @param l a list of lists objects
 * @return a list which conatains each first element of 
 * a list in the list given in argument
 **/

Object map_car(const Object& l);

/**
 * @brief Create a map symbol
 * @return An object which is a "map" symbol
 **/

Object lisp_map();

/**
 * @brief Apply a map function to a list of arguments
 * @param f the fonction applied
 * @param l the list which is evaluated
 * @param env the current environment
 * @return A list which contains the results of the mapping
 **/

Object do_map(const Object &f, const Object &l, Environment *env);