/**
 * @file apply.hh
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contains the definitions of the apply and apply_subr functions
 */

#include "object.hh"
#include "environment.hh"
#include "eval.hh"
#include "defs.hh"

/**
 * @brief Apply a function to its arguments
 * @param f The function to apply
 * @param lvals The parameters of the function
 * @param env The environment needed to execute the function
 * @return The object in wich the function was evaluated
 **/
Object apply(const Object &f, const Object &lvals, Environment *env);