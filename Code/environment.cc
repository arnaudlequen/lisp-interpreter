/**
 * @file environment.cc
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contain implementation of Environment class
 */

#include "environment.hh"
#include "defs.hh"
#include <stdexcept>
#include <cassert>
#include <string>

using namespace std;

Environment::Environment()
{
    a_list = nullptr;
    count = 0;
}

Environment::Environment(Environment *env)
{
    a_list = env->a_list; //Deep copy unnecessary
}

Object Environment::value(Object l)
{
    assert(symbolp(l));
    std::string symbol_value = Object_to_string(l);

    //We check if the symbol is bound
    for (Env_Cell* p = a_list; p != nullptr; p = next_cell(p))
    {
        Binding* binding = Object_to_binding(p);

        if (Binding_to_name(binding) == symbol_value){
            return Binding_to_Object(binding);
        }
    }
    //If the symbol hasn't been found in the environment, we check 
    //	if it is a native subroutine
    if (symbol_value == "+") return lisp_plus();

    else if (symbol_value == "-") return lisp_minus();

    else if (symbol_value == "*") return lisp_mult();

    else if (symbol_value == "/") return lisp_div();

    else if (symbol_value == "concat") return lisp_concat();

    else if (symbol_value == "car") return lisp_car();

    else if (symbol_value == "cdr") return lisp_cdr();

    else if (symbol_value == "cons") return lisp_cons();

    else if (symbol_value == "lambda") return lisp_lambda();

    else if (symbol_value == "=") return lisp_equal();

    else if (symbol_value == ">") return lisp_gt();

    else if (symbol_value == ">=") return lisp_geq();

    else if (symbol_value == "<") return lisp_lt();

    else if (symbol_value == "<=") return lisp_leq();

    throw std::string("Undefined variable: " + symbol_value);
}

void Environment::extend(const Object &symbol, const Object &value)
{
    std::string symbol_name = Object_to_string(symbol);

    extend(symbol_name, value);
}

void Environment::extend(std::string symbol_name, const Object &value)
{
    Binding* new_binding = create_binding(symbol_name, value);
    Env_Cell* new_cell = create_env_cell(new_binding, a_list);

    a_list = new_cell;

    count += 1;
}

void Environment::extend_assoc(const Object &symbols, 
							   const Object &values)
{
    if (!null(symbols) && !null(values))
    {
        extend(car(symbols), car(values));
        extend_assoc(cdr(symbols), cdr(values));
    }
}

void Environment::print()
{
    for (Env_Cell* p = a_list; p != nullptr; p = next_cell(p))
    {
        Binding* binding = Object_to_binding(p);

        std::string sym = Binding_to_name(binding);
        Object      val = Binding_to_Object(binding);

        cout << '"' << sym << '"' << " = " << val << "; ";
    }
}

int Environment::size()
{
    return count;
}
