/**
 * @file environment.hh
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contain definition of Environment class
 */

#ifndef __ENVIRONMENT_H__
#define __ENVIRONMENT_H__

#include "object.hh"
#include "cell.hh"
#include <vector>
#include <string>





class Environment
{
  public:
    Environment();

    Environment(Environment *env);

    /** 
     * @brief Give the object associate to the given symbol
     * @param l The symbol for wich we want to find the associated 
     * 			object
     * @return the value a symbol is bound to.
     * @note If the wanted item doesn't exist, this function will raise 
     * 			a string exception
     **/
    Object value(Object l);

    /**
     * @brief Add an item into the memory
     * @param symbol The symbol associated to the new object
     * @param value The object associated to the symbol
     **/
    void extend(const Object &symbol, const Object &value);

    /**
     * @brief Add an item into the memory
     * @param symbol The name of the symbol associated to the new object
     * @param value The object associated to the new symbol
     **/
    void extend(std::string symbol, const Object &value);

    /**
     * @brief Assign a set of objects to a set of symbols
     * @param symbols The name of the symbols associated to the new 
     * 				  objects
     * @param values The object associated to the symbol
     **/
    void extend_assoc(const Object &symbols, const Object &values);

    /**
     * @brief Print the content of environment
     * @return The generatesd object
     **/
    void print();

    /**
     * @brief Give the size of the environment
     * @return The size of the environment
     **/
    int size();

  private:
    Env_Cell* a_list;
    int count;
};

#endif //__ENVIRONMENT_H__
