/**
 * @file defs.cc
 * @version 1.0
 * @date 22 february 2018
 *
 * @brief Contain all do_, lisp_ and macro functions of the interpreter
 */

#include <string>
#include <iostream>
#include <cassert>
#include <cmath>

#include "object.hh"
#include "cell.hh"
#include "defs.hh"
#include "eval.hh"

using namespace std;


Object eval_list(const Object &l, Environment *env)
{
    if (null(l))
    {
        return nil();
    }
    else
    {
        Object cl = cdr(l);
        return cons(eval(car(l), env), eval_list(cl, env));
    }
}

Object lisp_eval()
{
    return symbol_to_Object("eval");
}

Object do_eval(const Object &l, Environment *env)
{
    return eval(eval(l, env), env);
}

Object lisp_apply() 
{
    return symbol_to_Object("apply");
}

Object do_apply(const Object &f, const Object &l, Environment *env)
{
    return eval(cons(f,l),env);
}


Object do_progn(Object &l, Environment *env)
{
    if (null(l))
    {
        return (nil());
    }
    else
    {
        if (null(cdr(l)))
        {
            return (eval(car(l), env));
        }
        else
        {
            Object cl = cdr(l);
            (eval (car(l),env));
            return (do_progn(cl, env));
        }
    }
}

Object do_if(Object &l, Environment *env)
{
    Object test_part = car(l);
    Object then_part = cadr(l);
    Object else_part = caddr(l);
    if (!(null(eval(test_part, env))))
    {
        return (eval(then_part, env));
    }
    else
    {
        return (eval(else_part, env));
    }
}

Object do_cond(Object &l, Environment *env)
{
    if (null(l))
    {
        return (nil());
    }
    else
    {
        Object test_part = car(car(l));
        Object then_part = cadr(car(l));
        if ((!(null(eval(test_part, env)))))
        {
            return (eval(then_part, env));
        }
        else
        {
            Object cl = cdr(l);
            return (do_cond(cl, env));
        }
    }
}

Object do_andthen(Object &l, Environment *env)
{
    Object part1 = car(l);
    Object part2 = cadr(l);
    if (!(null(eval(part1, env))))
    {
        return (eval(part2, env));
    }
    else
    {
        return (nil());
    }
}

Object list(const Object &x)
{
    return (cons(x, nil()));
}

Object list2(const Object &x,const Object &y)
{
    return (cons(x, list(y)));
}

Object list3(const Object &x, const Object &y, const Object &z)
{
    return (cons(x, list2(y, z)));
}

Object list4(const Object &x, const Object &y, const Object &z,
			 const Object &t)
{
    return (cons(x, list3(y, z, t)));
}

Object lisp_plus()
{
    return (subr_to_Object("+"));
}

Object do_plus(const Object &l, Environment *env)
{
    Object left_num = eval(car(l), env);
    Object right_num = eval(cadr(l), env);
    if (!(numberp(left_num) && numberp(right_num))) {
        throw string ("Can not compute without numbers!");
    }
    return (number_to_Object(Object_to_number(left_num) + 
			Object_to_number(right_num)));
}

Object lisp_minus()
{
    return (subr_to_Object("-"));
}

Object do_minus(const Object &l, Environment *env)
{
    Object left_num = eval(car(l), env);
    Object right_num = eval(cadr(l), env);
    if (!(numberp(left_num) && numberp(right_num))) {
        throw string ("Can not compute without numbers!");
    }
    return (number_to_Object(Object_to_number(left_num) - 
			Object_to_number(right_num)));
}

Object lisp_mult()
{
    return (subr_to_Object("*"));
}

Object do_mult(const Object &l, Environment *env)
{
    Object left_num = eval(car(l), env);
    Object right_num = eval(cadr(l), env);
    if (!(numberp(left_num) && numberp(right_num))){
        throw string ("Can not compute without numbers!");
    }
    return (number_to_Object(int(Object_to_number(left_num) * 
			Object_to_number(right_num))));
}

Object lisp_div()
{
    return (subr_to_Object("/"));
}

Object do_div(const Object &l, Environment *env)
{
    Object left_num = eval(car(l), env);
    Object right_num = eval(cadr(l), env);
    if (Object_to_number(right_num) == 0)
        throw string("Division by 0");
    if (!(numberp(left_num) && numberp(right_num))) {
        throw string ("Can not compute without numbers!");
    }
    return (number_to_Object(Object_to_number(left_num) / 
			Object_to_number(right_num)));
}

Object lisp_concat()
{
    return (subr_to_Object("concat"));
}

string do_concat_trim(const Object &l, Environment *env)
{
    if (null(l))
    {
        return ("");
    }
    else
    {
        string cr = Object_to_string(eval(car(l), env));
        string rs = do_concat_trim(cdr(l), env);

        cr = cr.substr(1, cr.size() - 2) + rs;
        return cr;
    }
}

Object do_concat(const Object &l, Environment *env)
{
    if (null(l))
    {
        return (string_to_Object(""));
    }
    else
    {
        return (string_to_Object("\"" + do_concat_trim(l, env) + "\""));
    }
}

Object lisp_car()
{
    return (subr_to_Object("car"));
}

Object do_car(const Object &l, Environment *env)
{
    return car(car(l));
}

Object lisp_cdr()
{
    return (subr_to_Object("cdr"));
}

Object do_cdr(const Object &l, Environment *env)
{
    return cdr(car(l));
}

Object lisp_cons()
{
    return (subr_to_Object("cons"));
}

Object do_cons(const Object &l, Environment *env)
{
    return (cons(car(l), cadr(l)));
}

Object lisp_eq()
{
    return (symbol_to_Object("eq"));
}

Object lisp_equal()
{
    return (subr_to_Object("="));
}

Object do_eq(const Object &l, Environment *env)
{
    if ((numberp(car(l)) || stringp(car(l))) &&
        (numberp(cadr(l)) || stringp(cadr(l))))
    {
        return (bool_to_Object(eq(car(l), cadr(l))));
    }

    return (bool_to_Object(false));
}

Object lisp_gt()
{
    return (subr_to_Object(">"));
}

Object do_gt(const Object &l, Environment *env)
{
    if (numberp(car(l)) && numberp(cadr(l)))
    {
        return (bool_to_Object(Object_to_number(car(l)) > 
				Object_to_number(cadr(l))));
    }

    return (bool_to_Object(false));
}

Object lisp_geq()
{
    return (subr_to_Object(">="));
}

Object do_geq(const Object &l, Environment *env)
{
    if (numberp(car(l)) && numberp(cadr(l)))
    {
        return (bool_to_Object(Object_to_number(car(l)) >= 
				Object_to_number(cadr(l))));
    }

    return (bool_to_Object(false));
}

Object lisp_lt()
{
    return (subr_to_Object("<"));
}

Object do_lt(const Object &l, Environment *env)
{
    if (numberp(car(l)) && numberp(cadr(l)))
    {
        return (bool_to_Object(Object_to_number(car(l)) < 
				Object_to_number(cadr(l))));
    }

    return (bool_to_Object(false));
}

Object lisp_leq()
{
    return (subr_to_Object("<="));
}

Object do_leq(const Object &l, Environment *env)
{
    if (numberp(car(l)) && numberp(cadr(l)))
    {
        return (bool_to_Object(Object_to_number(car(l)) <= 
				Object_to_number(cadr(l))));
    }

    return (bool_to_Object(false));
}

Object do_equal(const Object &l, Environment *env)
{
    Object p = eval(car(l), env);
    Object q = eval(cadr(l), env);

    return (bool_to_Object(eq(p, q)));
}


Object lisp_read()
{
    return (symbol_to_Object("read"));
}

Object do_read()
{
    string r;
    cin >> r;

    char* p;
    int i = int(strtol(r.c_str(), &p, 10));

    if(r.empty() || ((!isdigit(r[0])) && (r[0] != '-') && (r[0] != '+')) 
				 || *p == 0)
        return string_to_Object(r) ;
        
    return number_to_Object(i);
}


//keywords
Object lisp_lambda()
{
    return (subr_to_Object("lambda"));
}

Object do_lambda(const Object &f, const Object &lvals, Environment *env)
{
    Object body = caddr(f);
    Object laprs = cadr(f);
    Environment *new_env = new Environment(env);
    new_env->extend_assoc(laprs, lvals);
    return eval(body, new_env);
}

Object lisp_quote()
{
    return (symbol_to_Object("quote"));
}

Object lisp_setq()
{
    return (symbol_to_Object("setq"));
}

Object lisp_define()
{
    return (symbol_to_Object("define"));
}

Object lisp_defun()
{
    return (symbol_to_Object("defun"));
}

Object do_defun(const Object &f,const Object &arg, const Object &op,
				Environment *env)
{
    Object lam = list3(string_to_Object("lambda"),arg, op);
    env->extend(f,lam);
    cout << "SET: " << Object_to_string(f) << " = " << lam << endl;
    return string_to_Object("");
}

Object lisp_if()
{
    return (symbol_to_Object("if"));
}

Object lisp_cond()
{
    return (symbol_to_Object("cond"));
}

Object lisp_andthen()
{
    return (symbol_to_Object("andthen"));
}

Object lisp_progn()
{
    return (symbol_to_Object("progn"));
}

Object lisp_print()
{
    return (symbol_to_Object("print"));
}

Object do_print(const Object &l, Environment* env) 
{
    return (eval(car(l),env));
}

Object lisp_newline()
{
    return (symbol_to_Object("newline"));
}

Object do_newline()
{
    cout << endl;
    return (nil());
}

Object lisp_printenv()
{
    return (symbol_to_Object("printenv"));
}

void printenv(Environment *env)
{
    env->print();
}

Object lisp_debug()
{
    return (symbol_to_Object("debug"));
}

Object lisp_load()
{
    return (symbol_to_Object("load"));
}

Object lisp_let()
{
    return (symbol_to_Object("let"));
}

Object lisp_map_car()
{
    return (symbol_to_Object("map_car"));
}

Object map_car(const Object &l)
{
    if (null(l))
    {
        return (l);
    }
    else
    {
        return (cons(car(car(l)), map_car(cdr(l))));
    }
}

Object map_cadr(const Object &l)
{
    if (null(l))
    {
        return (l);
    }
    else
    {
        return (cons(cadr(car(l)), map_cadr(cdr(l))));
    }
}

Object macro_let_to_lambda(const Object &obj, Environment *env)
{
    Object binding_list = cadr(obj);
    Object body = caddr(obj);
    Object lpars = map_car(binding_list);
    Object largs = map_cadr(binding_list);
    Object lambda = lisp_lambda();
    return eval((cons(list3(lambda, lpars, body), largs)),env);
}

Object lisp_map()
{
    return (symbol_to_Object("map"));
}

Object do_map(const Object &f, const Object &l, Environment *env)
{
    if (null(l))
    {
        return (l);
    }
    else
    {
        return (cons (do_apply(f,car(l),env), do_map(f,cdr(l),env)));
    }
}

Object lisp_end()
{
    return symbol_to_Object("end");
}

Object do_end()
{
    cout << "Fin de Lisptoire !" << endl;
    exit(0);
    return (string_to_Object(""));
}

Object lisp_null()
{
    return (symbol_to_Object("null"));
}

Object do_null(const Object &l)
{
    if (null(l))
    {
        return (t());
    }
    else 
    {
        return (nil());
    }
}
    

Object lisp_stringp()
{
    return (symbol_to_Object("stringp"));
}

Object do_stringp(const Object &l)
{
    if (stringp(l))
    {
        return (t());
    }
    else
    {
        return (nil());
    }
}

Object lisp_numberp()
{
    return (symbol_to_Object("numberp"));
}

Object do_numberp(const Object &l)
{
    if (numberp(l))
    {
        return (t());
    }
    return (nil());
}

Object lisp_symbolp()
{
    return (symbol_to_Object("symbolp"));
}

Object do_symbolp(const Object &l)
{
    if (symbolp(l))
    {
        return (t());
    }
    return (nil());
}

Object lisp_listp()
{
    return (symbol_to_Object("listp"));
}

Object do_listp(const Object &l)
{
    if (listp(l))
    {
        return (t());
    }
    return (nil());
}
