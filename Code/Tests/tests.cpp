#include "tests.hpp"

#include <iostream>
#include "../defs.hh"
#include "../object.hh"
#include "../read.hh"
#include "../apply.hh"
#include "../environment.hh"
#include "../eval.hh"


using namespace std;

#define TEST(a,b) a?(cout << b << " Pass" << endl):(cout << b << " Fail" << endl)


Tests::Tests()
{
    
}

//eval
void Tests::Test_eval()
{
    cout << "Tests for the function \"eval\" :" << endl;
    Test_eval_nil();
    Test_eval_true();
    Test_eval_string();
    Test_eval_number();
    Test_eval_symbol();
    Test_eval_lambda();
    Test_eval_let();
    Test_eval_printenv();
    Test_eval_quote();
    Test_eval_if();
    Test_eval_cond();
    Test_eval_andthen();
    Test_eval_progn();
    Test_eval_setq();
}
void Tests::Test_eval_nil()
{
    TEST(eq(eval(nil(), new Environment()), nil()),"Test eval nil 1 :");
    TEST(!eq(eval(number_to_Object(57), new Environment()), nil()),"Test eval nil 2 :");
    TEST(!eq(eval(t(), new Environment()), nil()),"Test eval nil 3 :");
    cout << endl;
}

void Tests::Test_eval_true()
{
    TEST(eq(eval(t(), new Environment()), t()),"Test eval true 1 :");
    TEST(!eq(eval(number_to_Object(57), new Environment()), t()),"Test eval true 2 :");
    TEST(!eq(eval(nil(), new Environment()), t()),"Test eval true 3 :");
    cout << endl;
}

void Tests::Test_eval_string()
{
    TEST(eq(eval(string_to_Object("test"), new Environment()), string_to_Object("test")),
                    "Test eval string 1 :");
    TEST(!eq(eval(string_to_Object("test"), new Environment()), string_to_Object("tes")),
                    "Test eval string 2 :");
    TEST(!eq(eval(symbol_to_Object("test"), new Environment()), string_to_Object("test")),
                    "Test eval string 3 :");
    TEST(!eq(eval(number_to_Object(45), new Environment()), string_to_Object("45")),
                    "Test eval string 4 :");
    cout << endl;
}

void Tests::Test_eval_number()
{
    TEST(eq(eval(number_to_Object(45), new Environment()), number_to_Object(45)),
                    "Test eval number 1");
    TEST(!eq(eval(number_to_Object(40), new Environment()), number_to_Object(45)),
                    "Test eval number 2");
    TEST(!eq(eval(string_to_Object("45"), new Environment()), number_to_Object(45)),
                    "Test eval number 3");
    cout << endl;
}

void Tests::Test_eval_symbol()
{
    Environment* env =  new Environment();
    env->extend("test", symbol_to_Object("test"));
    env->extend("tes",symbol_to_Object("tes"));
    
    TEST(eq(eval(symbol_to_Object("test"), env), symbol_to_Object("test")),
                    "Test eval symbol 1 :");
    TEST(!eq(eval(symbol_to_Object("test"), env), symbol_to_Object("tes")),
                    "Test eval symbol 2 :");
    TEST(!eq(eval(string_to_Object("test"), env), symbol_to_Object("test")),
                    "Test eval symbol 3 :");
    TEST(!eq(eval(number_to_Object(45), env), symbol_to_Object("45")),
                    "Test eval symbol 4 :");
    cout << endl;
}
void Tests::Test_eval_lambda(){}
void Tests::Test_eval_let(){}
void Tests::Test_eval_printenv()
{
    //cout << "Unable to test eval printenv" << endl;
    //cout << endl;
}
void Tests::Test_eval_quote(){}
void Tests::Test_eval_if(){}
void Tests::Test_eval_cond(){}
void Tests::Test_eval_andthen(){}
void Tests::Test_eval_progn(){}
void Tests::Test_eval_setq(){}