#ifndef __TESTS_H__
#define __TESTS_H__

class Tests
{
    public:
        Tests();

        //eval
        static void Test_eval();
        static void Test_eval_nil();
        static void Test_eval_true();
        static void Test_eval_string();
        static void Test_eval_number();
        static void Test_eval_symbol();
        static void Test_eval_lambda();
        static void Test_eval_let();
        static void Test_eval_printenv();
        static void Test_eval_quote();
        static void Test_eval_if();
        static void Test_eval_cond();
        static void Test_eval_andthen();
        static void Test_eval_progn();
        static void Test_eval_setq();

        //Apply
        static bool Test_apply();

    private:
        
};

#endif //__TESTS_H__