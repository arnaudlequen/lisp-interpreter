.SUFFIXES:
.SUFFIXES: .l .y .cc .cpp .hpp .hh .o .c .h .orig .depend
.PHONY: clean all syntax astyle enscript geany emacs atom

default: all
all: lisp

lisp: clean
	@(cd Code && $(MAKE) lisp)
	@cp Code/lisp lisp

test: lisp
	@(cd Code/Tests && sh tests.sh)

demo: lisp
	@(cd Demo && sh demo.sh)

doc: lisp
	@doxygen Doxyfile

clean:
	@(cd Code && $(MAKE) clean)
	@rm -rf Doc
	@rm -rf lisp
